//
//  ViewController.m
//  Hello World
//
//  Created by Andrew Williams on 1/19/17.
//  Copyright © 2017 Andrew Williams. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UIButton *myButton;


@end

@implementation ViewController

-(IBAction)myButton {
        /*NSString *message = @"Hello Andrew";*/
    if ([self.messageLabel.text isEqualToString:@"HelloWorld"]) {
        self.messageLabel.text = @"Hello Andrew";
    } else {
        self.messageLabel.text = @"Hello World";
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
