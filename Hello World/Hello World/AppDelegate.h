//
//  AppDelegate.h
//  Hello World
//
//  Created by Andrew Williams on 1/19/17.
//  Copyright © 2017 Andrew Williams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

